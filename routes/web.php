<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'userController@index')->name('default');
Route::get('/dashboard', 'adminController@index');
Route::get('/profile', 'adminController@profile');
Route::get('/editprofile', 'adminController@editprofile');
// Hotel
Route::get('/allhotel', 'adminController@allhotel');
Route::get('/addhotel', 'adminController@addhotel');
Route::post('/addhotel', 'adminController@addhotel_exploria');
Route::get('/lihathotel/{id_hotel}/edit', 'adminController@updatehotel1');
Route::put('/lihathotel/{id_hotel}', 'adminController@updatehotel2');
Route::delete('/lihathotel/{id_hotel}', 'adminController@deletehotel');
// Bookings
Route::get('/allbookings', 'adminController@allbookings');
Route::get('/addbookings', 'adminController@addbookings');
Route::post('/addbookings', 'adminController@addbookings_exploria');
Route::get('/lihatbookings/{id_pengunjung}/edit', 'adminController@updatebookings1');
Route::put('/lihatbookings/{id_pengunjung}', 'adminController@updatebookings2');
Route::delete('/lihatbookings/{id_pengunjung}', 'adminController@deletebookings');
// Facility
Route::get('/allfacility', 'adminController@allfacility');
Route::get('/addfacility', 'adminController@addfacility');
Route::post('/addfacility', 'adminController@addfacility_exploria');
Route::get('/lihatfacility/{id_fasilitas}/edit', 'adminController@updatefacility1');
Route::put('/lihatfacility/{id_fasilitas}', 'adminController@updatefacility2');
Route::delete('/lihatfacility/{id_fasilitas}', 'adminController@deletefacility');
// Staff
Route::get('/allstaff', 'adminController@allstaff');
Route::get('/addstaff', 'adminController@addstaff');
Route::post('/addstaff', 'adminController@addstaff_exploria');
Route::get('/lihatstaff/{id_staff}/edit', 'adminController@updatestaff1');
Route::put('/lihatstaff/{id_staff}', 'adminController@updatestaff2');
Route::delete('/lihatstaff/{id_staff}', 'adminController@deletestaff');
// Admin
Route::get('/alladmin', 'adminController@alladmin');
Route::get('/addadmin', 'adminController@addadmin');
Route::post('/addadmin', 'adminController@addadmin_exploria');
Route::get('/lihatadmin/{id_admin}/edit', 'adminController@updateadmin1');
Route::put('/lihatadmin/{id_admin}', 'adminController@updateadmin2');
Route::delete('/lihatadmin/{id_admin}', 'adminController@deleteadmin');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
