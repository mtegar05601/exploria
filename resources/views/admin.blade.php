<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{ asset ('images/favicon.ico')}}" type="image/ico" />

  <title>EXPLORIA | </title>
  @stack('style')
  <!-- Bootstrap -->
  <link href="  {{ asset ('admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Font Awesome -->
  <link href="  {{ asset ('admin/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- NProgress -->
  <link href="  {{ asset ('admin/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
  <!-- iCheck -->
  <link href="  {{ asset ('admin/vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">

  <!-- bootstrap-progressbar -->
  <link href="  {{ asset ('admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
  <!-- JQVMap -->
  <link href="  {{ asset ('admin/vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet" />
  <!-- bootstrap-daterangepicker -->
  <link href="  {{ asset ('admin/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="  {{ asset ('admin/build/css/custom.min.css')}}" rel="stylesheet">

</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Dashboard</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="{{ asset ('admin/production/images/img.jpg')}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2>Admin</h2>
            </div>
          </div>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>General</h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-building"></i> Hotel <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="/allhotel">All Hotel</a></li>
                    <li><a href="/addhotel">Add Hotel</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-suitcase"></i> Booking <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="/allbookings">All Bookings</a></li>
                    <li><a href="/addbookings">Add Bookings</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-home"></i> Facility <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="/allfacility">All Facility</a></li>
                    <li><a href="/addfacility">Add Facility</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="menu_section">
              <h3>Internal</h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-users"></i> Staff <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="/allstaff">All Staff</a></li>
                    <li><a href="/addstaff">Add Staff</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-user"></i> Admin <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="/alladmin">All Admin</a></li>
                    <li><a href="/addadmin">Add Admin</a></li>
                  </ul>
                </li>
              </ul>
            </div>

            <div class="menu_section">
              <h3>Pages</h3>
              <ul class="nav side-menu">
                <li><a><i class="/index"></i> Open Pages </a></li>
                <li><a><i class="/logout"></i> Logout </a></li>

              </ul>
            </div>

          </div>
          <!-- /sidebar menu -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
          </div>
          <nav class="nav navbar-nav">
            <ul class=" navbar-right">
              <li class="nav-item dropdown open" style="padding-left: 15px;">
                @isset(Auth::user()->name)
                <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                  <img src="{{ asset ('admin/production/images/img.jpg')}}" alt="">{{Auth::user()->name}}
                </a>
                @else
                <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false" href="/login">
                  <img src="{{ asset ('admin/production/images/img.jpg')}}" alt="">Login
                </a>
                @endisset
                <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="/profile"> Profile</a>
                  <a class="dropdown-item" href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>

                </div>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">

        <div class="konten">
          @yield('konten')
        </div>
        <!-- top tiles -->

        <!-- /top tiles -->


        <br />


      </div>
      <!-- /page content -->
      <!-- footer content -->
      <footer>
        <div class="pull-right">
          Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
    </div>
  </div>
  @stack('scripts')
  <!-- jQuery -->
  <script src="  {{ asset ('admin/vendors/jquery/dist/jquery.min.js')}}">
  </script>
  <!-- Bootstrap -->
  <script src="  {{ asset ('admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <!-- FastClick -->
  <script src="  {{ asset ('admin/vendors/fastclick/lib/fastclick.js')}}"></script>
  <!-- NProgress -->
  <script src="  {{ asset ('admin/vendors/nprogress/nprogress.js')}}"></script>
  <!-- Chart.js -->
  <script src="  {{ asset ('admin/vendors/Chart.js/dist/Chart.min.js')}}"></script>
  <!-- gauge.js -->
  <script src="  {{ asset ('admin/vendors/gauge.js/dist/gauge.min.js')}}"></script>
  <!-- bootstrap-progressbar -->
  <script src="  {{ asset ('admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
  <!-- iCheck -->
  <script src="  {{ asset ('admin/vendors/iCheck/icheck.min.js')}}"></script>
  <!-- Skycons -->
  <script src="  {{ asset ('admin/vendors/skycons/skycons.js')}}"></script>
  <!-- Flot -->
  <script src="  {{ asset ('admin/vendors/Flot/jquery.flot.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/Flot/jquery.flot.pie.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/Flot/jquery.flot.time.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/Flot/jquery.flot.stack.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/Flot/jquery.flot.resize.js')}}"></script>
  <!-- Flot plugins -->
  <script src="  {{ asset ('admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/flot.curvedlines/curvedLines.js')}}"></script>
  <!-- DateJS -->
  <script src="  {{ asset ('admin/vendors/DateJS/build/date.js')}}"></script>
  <!-- JQVMap -->
  <script src="  {{ asset ('admin/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
  <!-- bootstrap-daterangepicker -->
  <script src="  {{ asset ('admin/vendors/moment/min/moment.min.js')}}"></script>
  <script src="  {{ asset ('admin/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

  <!-- Custom Theme Scripts -->
  <script src="  {{ asset ('admin/build/js/custom.min.js')}}"></script>

</body>

</html>