@extends('../admin')

<!-- I Made Kamajaya Ariestuta (203140714111006) -->

@section('konten')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>Ubah Data Pengunjung: {{$pilihfacility->nama_fasilitas}}</h2>
<form action="/lihatfacility/{{$pilihfacility->id_fasilitas}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="nama_fasilitas">Fasilitas</label>
            <input type="text" class="form-control" id="nama_fasilitas" name="nama_fasilitas" value="{{$pilihfacility->nama_fasilitas}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Upload Facility Image</label>
            <input type="file" class="form-control-file" id="gambar_fasilitas" name="gambar_fasilitas" value="{{$pilihfacility->gambar_fasilitas}}">
        </div>
        <div class="form-group">
            <label for="keterangan_fasilitas">Keterangan</label>
            <input type="text" class="form-control" id="keterangan_fasilitas" name="keterangan_fasilitas" value="{{$pilihfacility->keterangan_fasilitas}}">
        </div>
    </div>
    <div class="card ml-3 mr-3 mb-1">
        <button type="submit" class="btn btn-primary btn-user btn-block" value="Ubah Data">Ubah Data</button>
    </div>
</form>
@endsection
<!-- I Made Kamajaya Ariestuta (203140714111006) -->