@extends('../admin')

@section('konten')

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Hotel Data<small>exploria</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- <h1 class="h3 mb-4 text-gray-800">Tambah Hotel</h1> -->
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <form action="/addhotel" method="POST">
                    <p>For adding hotel data in <code>exploria</code> correctly at <a href="form.html"> form page</a></p>
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_hotel">Hotel Name</label>
                            <input type="text" class="form-control" id="nama_hotel" name="nama_hotel" required="required">
                        </div>
                        <div class="form-group">
                            <label for="harga_hotel">Hotel Price</label>
                            <input type="text" class="form-control" id="harga_hotel" name="harga_hotel" required="required">
                        </div>
                        <div class="form-group">
                            <label for="kota_hotel">City</label>
                            <input type="text" class="form-control" id="kota_hotel" name="kota_hotel" required="required">
                        </div>
                        <div class="form-group">
                            <label for="nama_fasilitas">Facility</label>
                            <input type="text" class="form-control" id="nama_fasilitas" name="nama_fasilitas" required="required">
                        </div>
                        <div class="form-group">
                            <label for="keterangan_hotel">Notes</label>
                            <input type="text" class="form-control" id="keterangan_hotel" name="keterangan_hotel" required="required">
                        </div>
                    </div>
                    <div class=" ml-3 mr-3 mb-1">
                        <button type="submit" class="btn btn-success">Add Hotel</button>

                        <a class="btn btn-secondary" href="/allhotel" role="button">See All Hotel Data</a>
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>


@endsection()