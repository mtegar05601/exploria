@extends('../admin')

<!-- I Made Kamajaya Ariestuta (203140714111006) -->

@section('konten')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>Ubah Data Hotel: {{$pilihhotel->nama_hotel}}</h2>


<form action="/lihathotel/{{$pilihhotel->id_hotel}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="nama_hotel">Nama Hotel</label>
            <input type="text" class="form-control" id="nama_hotel" name="nama_hotel" value="{{$pilihhotel->nama_hotel}}">
        </div>
        <div class="form-group">
            <label for="harga_hotel">Harga Hotel</label>
            <input type="text" class="form-control" id="harga_hotel" name="harga_hotel" value="{{$pilihhotel->harga_hotel}}">
        </div>
        <div class="form-group">
            <label for="kota_hotel">Kota</label>
            <input type="text" class="form-control" id="kota_hotel" name="kota_hotel" value="{{$pilihhotel->kota_hotel}}">
        </div>
        <div class="form-group">
            <label for="nama_fasilitas">Fasilitas</label>
            <input type="text" class="form-control" id="nama_fasilitas" name="nama_fasilitas" value="{{$pilihhotel->nama_fasilitas}}">
        </div>
        <div class="form-group">
            <label for="keterangan_hotel">Keterangan</label>
            <input type="text" class="form-control" id="keterangan_hotel" name="keterangan_hotel" value="{{$pilihhotel->keterangan_hotel}}">
        </div>
    </div>
    <div class="card ml-3 mr-3 mb-1">
        <button type="submit" class="btn btn-primary btn-user btn-block" value="Ubah Data">Ubah Data</button>
    </div>
</form>
@endsection
<!-- I Made Kamajaya Ariestuta (203140714111006) -->