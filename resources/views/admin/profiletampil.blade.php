@extends('../admin')

@section('konten')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>Ubah Data Admin</h2>
<form action="" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class=" form-group">
            <label for="nama_admin">Nama</label>
            <input type="text" class="form-control" id="nama_admin" name="nama_admin" value="">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="text" class="form-control" id="password" name="password" value="">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Foto</label>
            <input type="file" class="form-control-file" id="foto_admin" name="foto_admin" value">
        </div>
        <div class="form-group">
            <label for="keterangan_admin">Keterangan</label>
            <input type="text" class="form-control" id="keterangan_admin" name="keterangan_admin" value="">
        </div>
    </div>
    <div class="card ml-3 mr-3 mb-1">
        <button type="submit" class="btn btn-primary btn-user btn-block" value="Ubah Data">Ubah Data</button>
    </div>
</form>
@endsection
<!-- I Made Kamajaya Ariestuta (203140714111006) -->