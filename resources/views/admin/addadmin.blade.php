@extends('../admin')

@section('konten')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Admin Data<small>exploria</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <form action="/addadmin" method="POST" enctype="multipart/form-data">
                    <p>For adding admin data in <code>exploria</code> correctly at <a href="form.html"> form page</a></p>
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_admin">Admin Name</label>
                            <input type="text" class="form-control" id="nama_admin" name="nama_admin" required="required">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" required="required">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required="required">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Upload Admin Image</label>
                            <input type="file" class="form-control-file" id="foto_admin" name="foto_admin">
                        </div>
                        <div class="form-group">
                            <label for="keterangan_admin">Notes</label>
                            <input type="keterangan_admin" class="form-control" id="keterangan_admin" name="keterangan_admin" required="required">
                        </div>
                    </div>
                    <div class=" ml-3 mr-3 mb-1">
                        <button type="submit" class="btn btn-success">Add Admin</button>
                        <a class="btn btn-secondary" href="/alladmin" role="button">See All Admin Data</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection()