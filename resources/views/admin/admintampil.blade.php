@extends('../admin')

<!-- I Made Kamajaya Ariestuta (203140714111006) -->

@section('konten')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>Ubah Data Pengunjung: {{$pilihadmin->nama_admin}}</h2>
<form action="/lihatadmin/{{$pilihadmin->id_admin}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class=" form-group">
            <label for="nama_admin">Nama</label>
            <input type="text" class="form-control" id="nama_admin" name="nama_admin" value="{{$pilihadmin->nama_admin}}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="{{$pilihadmin->email}}">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="text" class="form-control" id="password" name="password" value="{{$pilihadmin->password}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Foto</label>
            <input type="file" class="form-control-file" id="foto_admin" name="foto_admin" value="{{$pilihadmin->foto_admin}}">
        </div>
        <div class="form-group">
            <label for="keterangan_admin">Keterangan</label>
            <input type="text" class="form-control" id="keterangan_admin" name="keterangan_admin" value="{{$pilihadmin->keterangan_admin}}">
        </div>
    </div>
    <div class="card ml-3 mr-3 mb-1">
        <button type="submit" class="btn btn-primary btn-user btn-block" value="Ubah Data">Ubah Data</button>
    </div>
</form>
@endsection
<!-- I Made Kamajaya Ariestuta (203140714111006) -->