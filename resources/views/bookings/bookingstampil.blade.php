@extends('../admin')

<!-- I Made Kamajaya Ariestuta (203140714111006) -->

@section('konten')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>Ubah Data Pengunjung: {{$pilihpengunjung->nama_pengunjung}}</h2>


<form action="/lihatbookings/{{$pilihpengunjung->id_pengunjung}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="nama_pengunjung">Nama</label>
            <input type="text" class="form-control" id="nama_pengunjung" name="nama_pengunjung" value="{{$pilihpengunjung->nama_pengunjung}}">
        </div>
        <div class="form-group">
            <label for="nama_hotel">Hotel</label>
            <input type="text" class="form-control" id="nama_hotel" name="nama_hotel" value="{{$pilihpengunjung->nama_hotel}}">
        </div>
        <div class="form-group">
            <label for="malam">Malam</label>
            <input type="text" class="form-control" id="malam" name="malam" value="{{$pilihpengunjung->malam}}">
        </div>
        <div class="form-group">
            <label for="no_telp">No Telp</label>
            <input type="text" class="form-control" id="no_telp" name="no_telp" value="{{$pilihpengunjung->no_telp}}">
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="text" class="form-control" id="alamat" name="alamat" value="{{$pilihpengunjung->alamat}}">
        </div>
        <div class="form-group">
            <label for="keterangan_pengunjung">Keterangan</label>
            <input type="text" class="form-control" id="keterangan_pengunjung" name="keterangan_pengunjung" value="{{$pilihpengunjung->keterangan_pengunjung}}">
        </div>
    </div>
    <div class="card ml-3 mr-3 mb-1">
        <button type="submit" class="btn btn-primary btn-user btn-block" value="Ubah Data">Ubah Data</button>
    </div>
</form>
@endsection
<!-- I Made Kamajaya Ariestuta (203140714111006) -->