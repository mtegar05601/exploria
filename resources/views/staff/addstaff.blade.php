@extends('../admin')

@section('konten')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Staff Data<small>exploria</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <form action="/addstaff" method="POST">
                    <p>For adding staff data in <code>exploria</code> correctly at <a href="form.html"> form page</a></p>
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_staff">Staff Name</label>
                            <input type="text" class="form-control" id="nama_staff" name="nama_staff" required="required">
                        </div>
                        <div class="form-group">
                            <label for="nama_hotel">From Hotel</label>
                            <input type="text" class="form-control" id="nama_hotel" name="nama_hotel" required="required">
                        </div>
                        <div class="form-group">
                            <label for="keterangan_staff">Notes</label>
                            <input type="text" class="form-control" id="keterangan_staff" name="keterangan_staff" required="required">
                        </div>
                    </div>
                    <div class=" ml-3 mr-3 mb-1">
                        <button type="submit" class="btn btn-success">Add Hotel</button>
                        <a class="btn btn-secondary" href="/allstaff" role="button">See All Hotel Data</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection()