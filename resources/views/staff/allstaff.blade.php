@extends('../admin')

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/b-2.2.2/b-colvis-2.2.2/b-html5-2.2.2/b-print-2.2.2/datatables.min.css" />
@endpush

@section('konten')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>All Staff Data<small>exploria</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <table border="1" class="table table-bordered table-striped" id="allstaff">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Hotel</th>
                            <th>Notes</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($liststaff)
                        @foreach($liststaff as $item=>$value)
                        <tr>
                            <td>{{$value->nama_staff}}</td>
                            <td>{{$value->nama_hotel}}</td>
                            <td>{{$value->keterangan_staff}}</td>
                            <td><a href="/lihatstaff/{{$value->id_staff}}/edit">edit <i class="fa fa-edit"></i> </a></td>
                            <form action="/lihatstaff/{{$value->id_staff}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <td tabindex="6">
                                    <button class="fa fa-trash">Delete</button>
                                </td>

                            </form>
                        </tr>
                        @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection()

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/b-2.2.2/b-colvis-2.2.2/b-html5-2.2.2/b-print-2.2.2/datatables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#allstaff').DataTable();
    });
</script>
<script>
    $('#allstaff').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'pdf', 'print', 'colvis'
        ]
    });
</script>
@endpush