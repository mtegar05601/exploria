<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class adminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function profile()
    {
        return view('profile');
    }
    public function index()
    {
        return view('admin');
    }
    public function editprofile()
    {
        return view('admin/profiletampil');
    }
    // Hotel
    public function allhotel()
    {
        $listhotel = DB::table('hotel')->get();
        return view('hotel/allhotel', compact('listhotel'));
    }
    public function addhotel()
    {
        return view('hotel/addhotel');
    }

    public function addhotel_exploria(Request $request)
    {
        $request->validate([
            'nama_hotel' => 'max:255',
            'harga_hotel' => 'max:255',
            'kota_hotel' => 'max:255',
            'nama_fasilitas' => 'max:255',
            'keterangan_hotel' => 'max:255',
        ]);
        DB::table('hotel')->insert([
            'nama_hotel' => $request['nama_hotel'],
            'harga_hotel' => $request['harga_hotel'],
            'kota_hotel' => $request['kota_hotel'],
            'nama_fasilitas' => $request['nama_fasilitas'],
            'keterangan_hotel' => $request['keterangan_hotel']
        ]);
        return redirect('/allhotel')->with('success', 'Data Hotel Berhasil Ditambahkan');
    }

    public function updatehotel1($id_hotel)
    {
        $pilihhotel = DB::table('hotel')->where('id_hotel', $id_hotel)->first();
        return view('hotel/hoteltampil', compact('pilihhotel'));
    }
    public function updatehotel2($id_hotel, Request $request)
    {
        $request->validate([
            'nama_hotel' => 'required',
            'harga_hotel' => 'required',
            'kota_hotel' => 'required',
            'nama_fasilitas' => 'required',
            'keterangan_hotel' => 'required',
        ]);
        $query = DB::table('hotel')->where('id_hotel', $id_hotel)
            ->update([
                'nama_hotel' => $request["nama_hotel"],
                'harga_hotel' => $request["harga_hotel"],
                'kota_hotel' => $request["kota_hotel"],
                'nama_fasilitas' => $request["nama_fasilitas"],
                'keterangan_hotel' => $request["keterangan_hotel"],
            ]);
        $nama_hotel = $request['nama_hotel'];
        $pesan = "Data hotel $nama_hotel berhasil diubah!";

        return redirect('/allhotel')->with('success', $pesan);
    }
    public function deletehotel($id_hotel)
    {
        DB::table('hotel')->where('id_hotel', $id_hotel)->delete();
        return redirect('/allhotel')->with('success', 'Data hotel berhasil dihapus');
    }
    // Bookings
    public function allbookings()
    {
        $listbookings = DB::table('bookings')->get();
        return view('bookings/allbookings', compact('listbookings'));
    }
    public function addbookings()
    {
        return view('bookings/addbookings');
    }
    public function addbookings_exploria(Request $request)
    {
        $request->validate([
            'nama_pengunjung' => 'max:255',
            'nama_hotel' => 'max:255',
            'malam' => 'max:255',
            'no_telp' => 'max:255',
            'alamat' => 'max:255',
            'keterangan_pengunjung' => 'max:255',
        ]);
        DB::table('bookings')->insert([
            'nama_pengunjung' => $request['nama_pengunjung'],
            'nama_hotel' => $request['nama_hotel'],
            'malam' => $request['malam'],
            'no_telp' => $request['no_telp'],
            'alamat' => $request['alamat'],
            'keterangan_pengunjung' => $request['keterangan_pengunjung']
        ]);
        return redirect('/allbookings')->with('success', 'Data Pengunjung Berhasil Ditambahkan');
    }
    public function updatebookings1($id_pengunjung)
    {
        $pilihpengunjung = DB::table('bookings')->where('id_pengunjung', $id_pengunjung)->first();
        return view('bookings/bookingstampil', compact('pilihpengunjung'));
    }
    public function updatebookings2($id_pengunjung, Request $request)
    {
        $request->validate([
            'nama_pengunjung' => 'required',
            'nama_hotel' => 'required',
            'malam' => 'required',
            'no_telp' => 'required',
            'alamat' => 'required',
            'keterangan_pengunjung' => 'required',
        ]);
        $query = DB::table('bookings')->where('id_pengunjung', $id_pengunjung)
            ->update([
                'nama_pengunjung' => $request["nama_pengunjung"],
                'nama_hotel' => $request["nama_hotel"],
                'malam' => $request["malam"],
                'no_telp' => $request["no_telp"],
                'alamat' => $request["alamat"],
                'keterangan_pengunjung' => $request["keterangan_pengunjung"],
            ]);
        $nama_pengunjung = $request['nama_pengunjung'];
        $pesan = "Data pengunjung $nama_pengunjung berhasil diubah!";

        return redirect('/allbookings')->with('success', $pesan);
    }
    public function deletebookings($id_pengunjung)
    {
        DB::table('bookings')->where('id_pengunjung', $id_pengunjung)->delete();
        return redirect('/allbookings')->with('success', 'Data pengunjung berhasil dihapus');
    }
    // Facility
    public function allfacility()
    {
        $listfacility = DB::table('facility')->get();
        return view('facility/allfacility', compact('listfacility'));
    }
    public function addfacility()
    {
        return view('facility/addfacility');
    }
    public function addfacility_exploria(Request $request)
    {
        $request->validate([
            'nama_fasilitas' => 'max:255',
            'gambar_fasilitas' => 'image|file',
            'keterangan_fasilitas' => 'max:255',
        ]);
        DB::table('facility')->insert([
            'nama_fasilitas' => $request['nama_fasilitas'],
            // 'gambar_fasilitas' => $request['gambar_fasilitas'],
            'gambar_fasilitas' => $request->file('gambar_fasilitas')->store('post-image'),
            'keterangan_fasilitas' => $request['keterangan_fasilitas']
        ]);
        return redirect('/allfacility')->with('success', 'Data Fasilitas Berhasil Ditambahkan');
    }
    public function updatefacility1($id_fasilitas)
    {
        $pilihfacility = DB::table('facility')->where('id_fasilitas', $id_fasilitas)->first();
        return view('facility/facilitytampil', compact('pilihfacility'));
    }
    public function updatefacility2($id_fasilitas, Request $request)
    {
        $request->validate([
            'nama_fasilitas' => 'required',
            'gambar_fasilitas' => 'image|file',
            'keterangan_fasilitas' => 'required',
        ]);
        $query = DB::table('facility')->where('id_fasilitas', $id_fasilitas)
            ->update([
                'nama_fasilitas' => $request["nama_fasilitas"],
                'gambar_fasilitas' => $request->file('gambar_fasilitas')->store('post-image'),
                'keterangan_fasilitas' => $request["keterangan_fasilitas"],
            ]);
        $nama_fasilitas = $request['nama_fasilitas'];
        $pesan = "Data fasilitas $nama_fasilitas berhasil diubah!";

        return redirect('/allfacility')->with('success', $pesan);
    }
    public function deletefacility($id_fasilitas)
    {
        DB::table('facility')->where('id_fasilitas', $id_fasilitas)->delete();
        return redirect('/allfacility')->with('success', 'Data fasilitas berhasil dihapus');
    }
    // Admin
    public function alladmin()
    {
        $listadmin = DB::table('admin')->get();
        return view('admin/alladmin', compact('listadmin'));
    }
    public function addadmin()
    {
        return view('admin/addadmin');
    }
    public function addadmin_exploria(Request $request)
    {
        $request->validate([
            'foto_admin' => 'image|file',
            'nama_admin' => 'max:255',
            'email' => 'max:255',
            'password' => 'max:255',
            'keterangan_admin' => 'max:255',
        ]);
        DB::table('admin')->insert([
            'foto_admin' => $request->file('foto_admin')->store('post-image'),
            'nama_admin' => $request['nama_admin'],
            'email' => $request['email'],
            'password' => $request['password'],
            'keterangan_admin' => $request['keterangan_admin'],
        ]);
        return redirect('/alladmin')->with('success', 'Data Admin Berhasil Ditambahkan');
    }
    public function updateadmin1($id_admin)
    {
        $pilihadmin = DB::table('admin')->where('id_admin', $id_admin)->first();
        return view('admin/admintampil', compact('pilihadmin'));
    }
    public function updateadmin2($id_admin, Request $request)
    {
        $request->validate([
            'foto_admin' => 'image|file',
            'nama_admin' => 'required',
            'email' => 'required',
            'password' => 'required',
            'keterangan_admin' => 'required',
        ]);
        $query = DB::table('admin')->where('id_admin', $id_admin)
            ->update([
                'foto_admin' => $request->file('foto_admin')->store('post-image'),
                'nama_admin' => $request["nama_admin"],
                'email' => $request["email"],
                'password' => $request["password"],
                'keterangan_admin' => $request["keterangan_admin"],
            ]);
        $nama_admin = $request['nama_admin'];
        $pesan = "Data fasilitas $nama_admin berhasil diubah!";

        return redirect('/alladmin')->with('success', $pesan);
    }
    public function deleteadmin($id_admin)
    {
        DB::table('admin')->where('id_admin', $id_admin)->delete();
        return redirect('/alladmin')->with('success', 'Data admin berhasil dihapus');
    }
    // Staff
    public function allstaff()
    {
        $liststaff = DB::table('staff')->get();
        return view('staff/allstaff', compact('liststaff'));
    }
    public function addstaff()
    {
        return view('staff/addstaff');
    }
    public function addstaff_exploria(Request $request)
    {
        $request->validate([
            'nama_staff' => 'max:255',
            'nama_hotel' => 'max:255',
            'keterangan_staff' => 'max:255',
        ]);
        DB::table('staff')->insert([
            'nama_staff' => $request['nama_staff'],
            'nama_hotel' => $request['nama_hotel'],
            'keterangan_staff' => $request['keterangan_staff'],
        ]);
        return redirect('/allstaff')->with('success', 'Data Fasilitas Berhasil Ditambahkan');
    }
    public function updatestaff1($id_staff)
    {
        $pilihstaff = DB::table('staff')->where('id_staff', $id_staff)->first();
        return view('staff/stafftampil', compact('pilihstaff'));
    }
    public function updatestaff2($id_staff, Request $request)
    {
        $request->validate([
            'nama_staff' => 'required',
            'nama_hotel' => 'required',
            'keterangan_staff' => 'required',
        ]);
        $query = DB::table('staff')->where('id_staff', $id_staff)
            ->update([
                'nama_staff' => $request["nama_staff"],
                'nama_hotel' => $request["nama_hotel"],
                'keterangan_staff' => $request["keterangan_staff"],
            ]);
        $nama_staff = $request['nama_staff'];
        $pesan = "Data staff $nama_staff berhasil diubah!";

        return redirect('/allstaff')->with('success', $pesan);
    }
    public function deletestaff($id_staff)
    {
        DB::table('staff')->where('id_staff', $id_staff)->delete();
        return redirect('/allstaff')->with('success', 'Data staff berhasil dihapus');
    }
}


// I Made Kamajaya Ariestuta (203140714111006)